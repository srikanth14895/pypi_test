from django.apps import AppConfig


class CdrfAppConfig(AppConfig):
    name = 'cdrf_app'
