import http.server, os
import socketserver
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
PORT = 8000
DIRECTORY = os.path.join(settings.BASE_DIR, "public")
os.chdir(DIRECTORY)

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        httpd = socketserver.TCPServer(("", PORT), http.server.SimpleHTTPRequestHandler)
        print("serving at port", PORT)
        httpd.serve_forever()
