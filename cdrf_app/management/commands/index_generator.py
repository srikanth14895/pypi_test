from django.core.management.base import BaseCommand, CommandError
from cdrf_app.build_tools.index_generator import main
class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        main()