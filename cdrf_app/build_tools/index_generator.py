#!/usr/bin/env python

import os
import json

from itertools import groupby

from cdrf_app.rest_framework_ccbv.inspector import drfklasses
from django.conf import settings


def main():
    d = {}
    klasses = sorted(list(drfklasses.values()), key=lambda x: x.__module__)

    for klass in klasses:
        app_name = klass.__module__.split('.')[0]
        module_name = klass.__module__.split('.')[1]
        if not app_name in d:
            d[app_name] = {}
        if not module_name in d[app_name]:
            d[app_name][module_name] = []
        d[app_name][module_name].append(klass.__name__)

    # if os.path.isfile('.klasses.json'):
    #     with open('.klasses.json', 'r') as f:
    #         d = json.loads(f.read())

    with open('.klasses.json', 'w') as f:
        json.dump(d, f, indent=2)


if __name__ == '__main__':
    main()
