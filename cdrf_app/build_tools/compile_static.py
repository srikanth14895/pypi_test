#!/usr/bin/env python

import os
import errno

from cdrf_app.rest_framework_ccbv.inspector import drfklasses
from cdrf_app.rest_framework_ccbv.renderers import (DetailPageRenderer,
                                           IndexPageRenderer,
                                           LandPageRenderer,
                                           ErrorPageRenderer,
                                           SitemapRenderer)
from distutils.dir_util import copy_tree


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def main(out_folder='public'):
    copy_tree("cdrf_app/static", out_folder+"/static")
    klasses = sorted(list(drfklasses.values()),
                     key=lambda x: (x.__module__, x.__name__))

    for klass in list(drfklasses.values()):
        renderer = DetailPageRenderer(klasses, klass.__name__,
                                      klass.__module__)
        mkdir_p(os.path.join(out_folder, klass.__module__))
        renderer.render(filename=os.path.join(out_folder, klass.__module__,
                                              klass.__name__ + '.html'))

    renderer = IndexPageRenderer(klasses)
    renderer.render(os.path.join(out_folder, 'index.html'))

    renderer = LandPageRenderer(klasses)
    renderer.render(os.path.join(out_folder, 'index.html'))
    renderer = ErrorPageRenderer(klasses)
    renderer.render(os.path.join(out_folder, 'error.html'))
    renderer = SitemapRenderer(klasses)
    renderer.render(os.path.join(out_folder, 'sitemap.xml'))

if __name__ == '__main__':
    main()
